enum ProductSizeType {
  s, m, l
}

extension ProductSizeTypeExtention on ProductSizeType {

  String get displayText {
    switch(this) {
      case ProductSizeType.s:
        return 'S';
      case ProductSizeType.m:
        return 'M';
      case ProductSizeType.l:
        return 'L';
    }
  }

  int get extraPrice {
    switch(this) {
      case ProductSizeType.s:
        return 0;
      case ProductSizeType.m:
        return 5000;
      case ProductSizeType.l:
        return 10000;
    }
  }
}