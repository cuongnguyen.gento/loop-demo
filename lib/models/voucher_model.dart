class VoucherModel {
  final String code;
  final String title;
  final DateTime endDate;

  VoucherModel({
    required this.code,
    required this.title,
    required this.endDate,
  });
}
