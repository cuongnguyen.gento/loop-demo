
class ProductMaterialModel {
  final String name;
  int quantity;
  final int price;

  ProductMaterialModel({
    required this.name,
    required this.quantity,
    required this.price,
  });

}
