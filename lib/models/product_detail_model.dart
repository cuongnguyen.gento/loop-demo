import 'package:loopdemo/models/product_ice_type.dart';
import 'package:loopdemo/models/product_material_model.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/models/product_sugar_type.dart';
import 'package:loopdemo/models/voucher_model.dart';

class ProductDetailModel {
  final String id;
  final String url;
  final String title;
  final int price;
  final int remain;
  final ProductSizeType sizeType;
  final ProductSugarType sugarType;
  final ProductIceType iceType;
  final int quantity;
  final List<ProductMaterialModel> materialModels;
  final List<VoucherModel> voucherModels;

  ProductDetailModel({
    required this.id,
    required this.url,
    required this.title,
    required this.price,
    required this.remain,
    required this.sizeType,
    required this.sugarType,
    required this.iceType,
    required this.quantity,
    required this.materialModels,
    required this.voucherModels,
  });
}
