class BriefProductModel {
  final String url;
  final String title;
  final int remain;
  final int price;

  BriefProductModel({
    required this.url,
    required this.title,
    required this.remain,
    required this.price,
  });
}
