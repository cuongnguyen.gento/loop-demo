enum ProductSugarType {
  hundred, seventy, thirty, zero
}

extension ProductSugarTypeExtention on ProductSugarType {
  String get displayText {
    switch(this) {
      case ProductSugarType.hundred:
        return '100';
      case ProductSugarType.seventy:
        return '70';
      case ProductSugarType.thirty:
        return '30';
      case ProductSugarType.zero:
        return '0';
    }
  }
}