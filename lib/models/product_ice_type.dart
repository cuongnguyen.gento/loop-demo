enum ProductIceType {
  hundred, seventy, thirty, zero
}

extension ProductIceTypeExtension on ProductIceType {
  String get displayText {
    switch(this) {
      case ProductIceType.hundred:
        return '100';
      case ProductIceType.seventy:
        return '70';
      case ProductIceType.thirty:
        return '30';
      case ProductIceType.zero:
        return '0';
    }
  }
}