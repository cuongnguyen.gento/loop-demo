import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/quantity_control.dart';

class ProductDetailQuantitySection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Container(
      color: AppColorManager.white,
      padding: EdgeInsets.symmetric(vertical: 8),
      alignment: Alignment.center,
      child: BlocBuilder<int>(
        stream: bloc.currentQuantityBehaviorSubject,
        builder: (context, quantity) {
          return QuantityControl(
            quantity: quantity,
            onTapMinusButton: bloc.onMinusQuantityButtonTap,
            onTapAddButton: bloc.onAddQuantityButtonTap,
          );
        },
      ),
    );
  }
}
