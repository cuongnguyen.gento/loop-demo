import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/note_icon.dart';

class ProductDetailNoteButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Container(
      height: 56,
      child: ElevatedButtonTheme(
        data: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: AppColorManager.white,
            onPrimary: AppColorManager.black,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
            elevation: 0,
          ),
        ),
        child: ElevatedButton(
          onPressed: bloc.onTapNoteButton,
          child: Row(
            children: [
              SizedBox(width: 34),
              NoteIcon(),
              SizedBox(width: 14),
              Expanded(
                child: Text(
                  'Thêm ghi chú món ăn',
                  style: AppTextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
