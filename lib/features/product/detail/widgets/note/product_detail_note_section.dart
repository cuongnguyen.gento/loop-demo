import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/note/product_detail_note_button.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/note_icon.dart';

class ProductDetailNoteSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Column(
        children: [
          ProductDetailNoteButton(),
          BlocBuilder<bool>(
            stream: bloc.showNoteInputBehaviorSubject,
            builder: (context, value) {
              if (value == false) {
                return Container();
              }
              return Container(
                height: 150,
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: AppColorManager.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    bottomRight: Radius.circular(8),
                  ),
                ),
                child: TextField(
                  expands: true,
                  maxLines: null,
                  minLines: null,
                  decoration: InputDecoration(
                    hintText: 'Nhập ghi chú của bạn',
                    hintStyle: AppTextStyle.disable(fontSize: 14),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(15),
                      borderSide: BorderSide(
                        width: 1,
                        color: AppColorManager.whiteSmoke,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
