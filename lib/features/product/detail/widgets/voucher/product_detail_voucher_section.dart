import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/voucher/product_detail_voucher_list_tile_info.dart';
import 'package:loopdemo/features/product/detail/widgets/voucher/product_detail_voucher_list_tile_select_button.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/clippers/right_side_sun_clipper.dart';
import 'package:loopdemo/shared/deviders/vertical_dash_divider.dart';

class ProductDetailVoucherSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final voucherModels = BlocProvider.of<ProductDetailBloc>(context)
        .productDetailModelBehaviorSubject
        .value
        .voucherModels;
    return Container(
      height: 84,
      child: ListView.separated(
        padding: EdgeInsets.symmetric(horizontal: 16),
        scrollDirection: Axis.horizontal,
        itemCount: voucherModels.length,
        itemBuilder: (context, index) {
          final voucherModel = voucherModels[index];
          return Row(
            children: [
              ProductDetailVoucherListTileInfo(voucherModel: voucherModel),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: VerticalDashDivider(),
              ),
              ProductDetailVoucherListTileSelectButton(),
            ],
          );
        },
        separatorBuilder: (context, index) {
          return SizedBox(width: 8);
        },
      ),
    );
  }
}
