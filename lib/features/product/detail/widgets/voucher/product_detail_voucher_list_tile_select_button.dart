import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/clippers/left_side_sun_clipper.dart';
import 'package:loopdemo/shared/clippers/right_side_sun_clipper.dart';

class ProductDetailVoucherListTileSelectButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: LeftSideSunClipper(),
      child: Container(
        decoration: BoxDecoration(
          color: AppColorManager.white,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(8),
            topRight: Radius.circular(8),
          ),
        ),
        child: Material(
          color: AppColorManager.transparent,
          child: InkWell(
            onTap: () {},
            child: Container(
              alignment: Alignment.center,
              color: AppColorManager.transparent,
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                'SỬ DỤNG',
                style: AppTextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
