import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:loopdemo/models/voucher_model.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/clippers/right_side_sun_clipper.dart';

class ProductDetailVoucherListTileInfo extends StatelessWidget {
  final VoucherModel voucherModel;

  const ProductDetailVoucherListTileInfo({Key? key, required this.voucherModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: RightSideSunClipper(),
      child: Container(
        padding: EdgeInsets.only(left: 22, right: 12),
        decoration: BoxDecoration(
          color: AppColorManager.white,
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(8),
            topLeft: Radius.circular(8),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              voucherModel.code,
              style: AppTextStyle(
                color: AppColorManager.dimGray,
                fontSize: 11,
                fontWeight: FontWeight.w500,
              ),
            ),
            Text(
              voucherModel.title,
              style: AppTextStyle(
                color: AppColorManager.black,
                fontSize: 13,
                fontWeight: FontWeight.w700,
              ),
            ),
            Text(
              DateFormat("dd/MM/yyyy").format(voucherModel.endDate),
              style: AppTextStyle.disable(fontSize: 11),
            ),
          ],
        ),
      ),
    );
  }
}
