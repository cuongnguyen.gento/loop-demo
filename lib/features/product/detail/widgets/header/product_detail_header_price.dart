import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/formatters/thounsand_formatter.dart';

class ProductDetailHeaderPrice extends StatelessWidget {
  final int price;

  const ProductDetailHeaderPrice({Key? key, required this.price}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
      decoration: BoxDecoration(
        color: AppColorManager.whiteSmoke,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(16),
          bottomLeft: Radius.circular(8),
        ),
      ),
      child: Text(
        '${ThousandFormatter.format(price)} đ',
        style: AppTextStyle(
          fontSize: 15,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
