import 'package:flutter/material.dart';
import 'package:loopdemo/features/product/detail/widgets/header/product_detail_header_price.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/cached_image.dart';

class ProductDetailHeaderSection extends StatelessWidget {
  final String url;
  final String title;
  final int price;
  final int remain;

  const ProductDetailHeaderSection({
    Key? key,
    required this.url,
    required this.title,
    required this.price,
    required this.remain,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 124,
      child: Row(
        children: [
          AspectRatio(
            aspectRatio: 1,
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12),
                bottomRight: Radius.circular(12),
              ),
              child: CachedImage(url: url),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text(
                    title,
                    style: AppTextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ProductDetailHeaderPrice(
                        price: price,
                      ),
                      Text(
                        '$remain còn lại',
                        style: AppTextStyle(
                          fontSize: 11,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
