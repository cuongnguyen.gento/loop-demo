import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class ProductDetailMaterialRemoveButton extends StatelessWidget {
  final VoidCallback onTap;

  const ProductDetailMaterialRemoveButton({Key? key, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColorManager.transparent,
      child: InkWell(
        onTap: onTap,
        customBorder: CircleBorder(),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            width: 20,
            height: 20,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: AppColorManager.oldLace,
              shape: BoxShape.circle,
            ),
            child: Icon(
              Icons.close,
              size: 6,
              color: AppColorManager.yellow,
            ),
          ),
        ),
      ),
    );
  }
}
