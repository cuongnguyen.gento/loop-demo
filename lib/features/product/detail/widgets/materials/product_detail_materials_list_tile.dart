import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/materials/product_detail_material_add_button.dart';
import 'package:loopdemo/features/product/detail/widgets/materials/product_detail_material_remove_button.dart';
import 'package:loopdemo/models/product_material_model.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/formatters/thounsand_formatter.dart';

class ProductDetailMaterialsListTile extends StatelessWidget {
  final int index;
  final ProductMaterialModel materialModel;

  const ProductDetailMaterialsListTile({
    Key? key,
    required this.index,
    required this.materialModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Container(
      height: 56,
      child: Row(
        children: [
          ProductDetailMaterialAddButton(
            onTap: () {
              bloc.onTapAddMaterialButton(index);
            },
            quantity: materialModel.quantity,
          ),
          Expanded(
            child: Text(
              materialModel.name,
              style: materialModel.quantity == 0
                  ? AppTextStyle.disable(fontSize: 15)
                  : AppTextStyle.enable(fontSize: 15),
            ),
          ),
          Text(
            '+${ThousandFormatter.format(materialModel.price)}',
            style: materialModel.quantity == 0
                ? AppTextStyle.disable(fontSize: 13)
                : AppTextStyle(fontSize: 13, fontWeight: FontWeight.w400),
          ),
          materialModel.quantity == 0
              ? SizedBox(width: 16)
              : Row(
                children: [
                  ProductDetailMaterialRemoveButton(
                      onTap: () => bloc.onTapRemoveMaterialButton(index),
                    ),
                  SizedBox(width: 8),
                ],
              ),
        ],
      ),
    );
  }
}
