import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/minus_icon.dart';

class ProductDetailMaterialAddButton extends StatelessWidget {

  final VoidCallback onTap;
  final int quantity;

  const ProductDetailMaterialAddButton({
    Key? key,
    required this.quantity,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColorManager.white,
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 44,
          width: 44,
          alignment: Alignment.center,
          child: quantity == 0
              ? MinusIcon()
              : Text(
            'x$quantity',
            style: AppTextStyle.highlight(fontSize: 13),
          ),
        ),
      ),
    );
  }
}
