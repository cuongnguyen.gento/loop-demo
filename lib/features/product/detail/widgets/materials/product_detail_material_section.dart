import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/materials/product_detail_materials_list_tile.dart';
import 'package:loopdemo/models/product_material_model.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/deviders/horizontal_divider.dart';

class ProductDetailMaterialSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: AppColorManager.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12),
        ),
      ),
      child: BlocBuilder<List<ProductMaterialModel>>(
        stream: bloc.currentMaterialModelsBehaviorSubject,
        builder: (context, materialModels) {
          return ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: materialModels.length,
            itemBuilder: (context, index) {
              return ProductDetailMaterialsListTile(
                index: index,
                materialModel: materialModels[index],
              );
            },
            separatorBuilder: (context, index) {
              if (index == materialModels.length) {
                return Container();
              }
              return Padding(
                padding: const EdgeInsets.only(left: 44, right: 16),
                child: HorizontalDivider(),
              );
            },
          );
        },
      ),
    );
  }
}
