import 'package:flutter/material.dart';
import 'package:loopdemo/features/product/detail/widgets/discount_and_order/product_detail_discount_button.dart';
import 'package:loopdemo/features/product/detail/widgets/discount_and_order/product_detail_order_button.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/discount_icon.dart';

class ProductDetailDiscountAndOrderSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColorManager.white,
      height: 80,
      child: Row(
        children: [
          Expanded(child: ProductDetailDiscountButton()),
          ProductDetailOrderButton(),
          SizedBox(width: 16),
        ],
      ),
    );
  }
}
