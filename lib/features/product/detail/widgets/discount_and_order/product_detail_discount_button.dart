import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/discount_icon.dart';

class ProductDetailDiscountButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColorManager.white,
      alignment: Alignment.center,
      child: Material(
        color: AppColorManager.white,
        child: InkWell(
          onTap: () {},
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                DiscountIcon(),
                SizedBox(height: 10),
                Text(
                  'Giảm giá',
                  style: AppTextStyle(
                    fontSize: 11,
                    fontWeight: FontWeight.w600,
                    color: AppColorManager.black,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
