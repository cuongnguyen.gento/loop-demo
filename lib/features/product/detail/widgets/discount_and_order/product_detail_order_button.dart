import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class ProductDetailOrderButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      child: ElevatedButtonTheme(
        data: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: AppColorManager.yellow,
            // onPrimary: foregroundColor,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
        ),
        child: ElevatedButton(
          onPressed: () {},
          child: DefaultTextStyle(
            style: AppTextStyle(
              color: AppColorManager.white,
              fontSize: 15,
              fontWeight: FontWeight.w600,
            ),
            child: Row(
              children: [
                Text('Đặt món'),
                SizedBox(width: 38),
                Text('500.000.000 - đ'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
