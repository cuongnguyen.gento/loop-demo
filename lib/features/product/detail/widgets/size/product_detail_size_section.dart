import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/size/product_detail_size_list.dart';
import 'package:loopdemo/features/product/detail/widgets/size/product_detail_size_list_tile.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/deviders/horizontal_divider.dart';

class ProductDetailSizeSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: AppColorManager.white,
        borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 16),
          Row(
            children: [
              SizedBox(width: 16),
              Text(
                'Kích cỡ',
                style: AppTextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
          ProductDetailSizeList(),
        ],
      ),
    );
  }
}
