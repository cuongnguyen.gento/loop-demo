import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/size/product_detail_size_list_tile.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/shared/deviders/horizontal_divider.dart';

class ProductDetailSizeList extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return BlocBuilder<ProductSizeType>(
      stream: bloc.selectedSizeTypeBehaviorSubject,
      builder: (context, selectedSizeType) {
        return ListView.separated(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: ProductSizeType.values.length,
          itemBuilder: (context, index) {
            final sizeType = ProductSizeType.values[index];
            return ProductDetailSizeListTile(
              index: index,
              sizeType: sizeType,
              selected: sizeType == selectedSizeType,
            );
          },
          separatorBuilder: (context, index) {
            if (index == ProductSizeType.values.length - 1) {
              return Container();
            }
            return Padding(
              padding: const EdgeInsets.only(left: 44, right: 16),
              child: HorizontalDivider(),
            );
          },
        );
      },
    );
  }
}
