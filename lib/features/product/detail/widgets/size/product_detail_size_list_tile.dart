import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/formatters/thounsand_formatter.dart';
import 'package:loopdemo/shared/widgets/icons/radio_icon.dart';

class ProductDetailSizeListTile extends StatelessWidget {
  final int index;
  final ProductSizeType sizeType;
  final bool selected;

  const ProductDetailSizeListTile({
    Key? key,
    required this.index,
    required this.sizeType,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColorManager.white,
      child: InkWell(
        onTap: () {
          BlocProvider.of<ProductDetailBloc>(context)
              .onSelectSizeType(selectedIndex: index);
        },
        child: Container(
          height: 56,
          child: Row(
            children: [
              SizedBox(width: 16),
              RadioIcon(selected: selected),
              SizedBox(width: 12),
              Text(
                sizeType.displayText,
                style: selected
                    ? AppTextStyle.enable(fontSize: 15)
                    : AppTextStyle.disable(fontSize: 15),
              ),
              Spacer(),
              Text(
                '+${ThousandFormatter.format(sizeType.extraPrice)}',
                style: selected
                    ? AppTextStyle.enable(fontSize: 15)
                    : AppTextStyle.disable(fontSize: 15),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
