import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/models/product_sugar_type.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/lists/single_selection_buttons_list.dart';

class ProductDetailSugarSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProductDetailBloc>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Đường',
          style: AppTextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w700,
          ),
        ),
        SizedBox(height: 12),
        BlocBuilder<ProductSugarType>(
          stream: bloc.selectedSugarTypeBehaviorSubject,
          builder: (context, selectedSugarType) {
            return SingleSelectionButtonsList(
              onTap: bloc.onSelectSugarType,
              selectedValue: SingleSelectionButton(
                  displayText: selectedSugarType.displayText),
              values: ProductSugarType.values.map((e) {
                return SingleSelectionButton(displayText: e.displayText);
              }).toList(),
            );
          },
        ),
      ],
    );
  }
}
