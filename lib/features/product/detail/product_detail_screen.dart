import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/widgets/discount_and_order/product_detail_discount_and_order_section.dart';
import 'package:loopdemo/features/product/detail/widgets/header/product_detail_header_section.dart';
import 'package:loopdemo/features/product/detail/widgets/ice/product_detail_ice_section.dart';
import 'package:loopdemo/features/product/detail/widgets/materials/product_detail_material_section.dart';
import 'package:loopdemo/features/product/detail/widgets/note/product_detail_note_section.dart';
import 'package:loopdemo/features/product/detail/widgets/quantity/product_detail_quantity_section.dart';
import 'package:loopdemo/features/product/detail/widgets/size/product_detail_size_section.dart';
import 'package:loopdemo/features/product/detail/widgets/sugar/product_detail_sugar_section.dart';
import 'package:loopdemo/features/product/detail/widgets/voucher/product_detail_voucher_section.dart';
import 'package:loopdemo/models/product_detail_model.dart';
import 'package:loopdemo/models/product_sugar_type.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/deviders/horizontal_divider.dart';
import 'package:loopdemo/shared/widgets/close_action_button.dart';
import 'package:loopdemo/shared/widgets/lists/single_selection_buttons_list.dart';
import 'package:loopdemo/shared/widgets/screen_layout.dart';

class ProductDetailScreen extends StatefulWidget {
  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  late final bloc = BlocProvider.of<ProductDetailBloc>(context);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
      bloc.initializeScreen();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ScreenLayout(
      showLoadingIndicatorBehaviorSubject:
          bloc.showLoadingIndicatorBehaviorSubject,
      body: Stack(
        children: [
          BlocBuilder<ProductDetailModel>(
            stream: bloc.productDetailModelBehaviorSubject,
            builder: (context, productDetailModel) {
              return SingleChildScrollView(
                child: Container(
                  child: Column(
                    children: [
                      ProductDetailHeaderSection(
                        url: productDetailModel.url,
                        title: productDetailModel.title,
                        price: productDetailModel.price,
                        remain: productDetailModel.remain,
                      ),
                      SizedBox(height: 24),
                      ProductDetailSizeSection(),
                      SizedBox(height: 16),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 16),
                        padding: EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: AppColorManager.white,
                        ),
                        child: Column(
                          children: [
                            ProductDetailSugarSection(),
                            SizedBox(height: 16),
                            ProductDetailIceSection(),
                          ],
                        ),
                      ),
                      ProductDetailQuantitySection(),
                      HorizontalDivider(),
                      ProductDetailDiscountAndOrderSection(),
                      ProductDetailMaterialSection(),
                      SizedBox(height: 24),
                      ProductDetailNoteSection(),
                      SizedBox(height: 16),
                      ProductDetailVoucherSection(),
                    ],
                  ),
                ),
              );
            },
          ),
          CloseActionButton(onTap: bloc.onTapCloseActionButton),
        ],
      ),
    );
  }
}
