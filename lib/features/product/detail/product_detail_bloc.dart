import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/models/product_detail_model.dart';
import 'package:loopdemo/models/product_ice_type.dart';
import 'package:loopdemo/models/product_material_model.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/models/product_sugar_type.dart';
import 'package:loopdemo/repositories/repository.dart';
import 'package:loopdemo/shared/routing_manager.dart';
import 'package:rxdart/rxdart.dart';

class ProductDetailBloc extends BlocBase {
  final String productId;
  final RoutingManager routingManager;
  final showLoadingIndicatorBehaviorSubject = BehaviorSubject.seeded(false);
  final productDetailModelBehaviorSubject = BehaviorSubject<ProductDetailModel>();
  final Repository repository;
  final selectedSizeTypeBehaviorSubject = BehaviorSubject<ProductSizeType>();
  final selectedSugarTypeBehaviorSubject = BehaviorSubject<ProductSugarType>();
  final selectedIceTypeBehaviorSubject = BehaviorSubject<ProductIceType>();
  final currentQuantityBehaviorSubject = BehaviorSubject<int>();
  final currentMaterialModelsBehaviorSubject = BehaviorSubject<List<ProductMaterialModel>>();
  final showNoteInputBehaviorSubject = BehaviorSubject.seeded(false);

  ProductDetailBloc({
    required this.productId,
    required this.routingManager,
    required this.repository,
  });

  @override
  void dispose() {
    showLoadingIndicatorBehaviorSubject.close();
    productDetailModelBehaviorSubject.close();
    selectedSizeTypeBehaviorSubject.close();
    selectedSugarTypeBehaviorSubject.close();
    selectedIceTypeBehaviorSubject.close();
    currentQuantityBehaviorSubject.close();
    currentMaterialModelsBehaviorSubject.close();
    showNoteInputBehaviorSubject.close();
    super.dispose();
  }

  void initializeScreen() async {
    showLoadingIndicatorBehaviorSubject.add(true);
    final productDetailModel = await repository.getProductDetail(productId: productId);
    selectedSizeTypeBehaviorSubject.add(productDetailModel.sizeType);
    selectedSugarTypeBehaviorSubject.add(productDetailModel.sugarType);
    selectedIceTypeBehaviorSubject.add(productDetailModel.iceType);
    currentQuantityBehaviorSubject.add(productDetailModel.quantity);
    currentMaterialModelsBehaviorSubject.add(productDetailModel.materialModels);
    productDetailModelBehaviorSubject.add(productDetailModel);
    showLoadingIndicatorBehaviorSubject.add(false);
  }

  void onTapCloseActionButton() {
    routingManager.pop();
  }

  void onSelectSizeType({required int selectedIndex}) {
    selectedSizeTypeBehaviorSubject.add(ProductSizeType.values[selectedIndex]);
  }

  void onSelectSugarType(int selectedIndex) {
    selectedSugarTypeBehaviorSubject.add(ProductSugarType.values[selectedIndex]);
  }

  void onSelectIceType(int selectedIndex) {
    selectedIceTypeBehaviorSubject.add(ProductIceType.values[selectedIndex]);
  }

  void onMinusQuantityButtonTap() {
    currentQuantityBehaviorSubject.add(currentQuantityBehaviorSubject.value - 1);
  }

  void onAddQuantityButtonTap() {
    currentQuantityBehaviorSubject.add(currentQuantityBehaviorSubject.value + 1);
  }

  void onTapAddMaterialButton(int index) {
    final currentMaterialModels = currentMaterialModelsBehaviorSubject.value;
    currentMaterialModels[index].quantity += 1;
    currentMaterialModelsBehaviorSubject.add(currentMaterialModels);
  }

  void onTapRemoveMaterialButton(int index) {
    final currentMaterialModels = currentMaterialModelsBehaviorSubject.value;
    currentMaterialModels[index].quantity = 0;
    currentMaterialModelsBehaviorSubject.add(currentMaterialModels);
  }

  void onTapNoteButton() {
    showNoteInputBehaviorSubject.add(!showNoteInputBehaviorSubject.value);
  }
}
