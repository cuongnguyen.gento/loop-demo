import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/models/home_filter_option_type.dart';
import 'package:loopdemo/models/brief_product_model.dart';
import 'package:loopdemo/repositories/repository.dart';
import 'package:loopdemo/shared/routing_manager.dart';
import 'package:rxdart/rxdart.dart';

class HomeBloc extends BlocBase {

  final Repository repository;

  final RoutingManager routingManager;
  final showLoadingIndicatorBehaviorSubject = BehaviorSubject.seeded(false);
  final briefProductModelsBehaviorSubject = BehaviorSubject<List<BriefProductModel>>();
  final selectedFilterOptionTypeBehaviorSubject =
      BehaviorSubject.seeded(HomeFilterOptionType.atStore);

  HomeBloc({required this.repository, required this.routingManager});

  @override
  void dispose() {
    showLoadingIndicatorBehaviorSubject.close();
    selectedFilterOptionTypeBehaviorSubject.close();
    briefProductModelsBehaviorSubject.close();
    super.dispose();
  }

  void onInitializeScreen() async {
    showLoadingIndicatorBehaviorSubject.add(true);
    final briefProductModels = await repository.getBriefProductModels();
    showLoadingIndicatorBehaviorSubject.add(false);
    briefProductModelsBehaviorSubject.add(briefProductModels);
  }

  void onTapAtStoreButton() {
    if (selectedFilterOptionTypeBehaviorSubject.value ==
        HomeFilterOptionType.atStore) {
      return;
    }
    selectedFilterOptionTypeBehaviorSubject.add(HomeFilterOptionType.atStore);
  }

  void onTapTakeAwayButton() {
    if (selectedFilterOptionTypeBehaviorSubject.value ==
        HomeFilterOptionType.takeAway) {
      return;
    }
    selectedFilterOptionTypeBehaviorSubject.add(HomeFilterOptionType.takeAway);
  }

  void onTapDeliveryButton() {
    if (selectedFilterOptionTypeBehaviorSubject.value ==
        HomeFilterOptionType.delivery) {
      return;
    }
    selectedFilterOptionTypeBehaviorSubject.add(HomeFilterOptionType.delivery);
  }

  void onTapBriefProduct(int index) {
    routingManager.push(RoutingPageType.productDetail(productId: index.toString()));
  }
}
