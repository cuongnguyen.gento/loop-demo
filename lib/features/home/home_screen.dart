import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/features/home/widgets/bottom_panel/home_bottom_panel.dart';
import 'package:loopdemo/features/home/widgets/home_app_bar.dart';
import 'package:loopdemo/features/home/widgets/home_appetizer_dropdown_button.dart';
import 'package:loopdemo/features/home/widgets/home_floating_button.dart';
import 'package:loopdemo/features/home/widgets/product/home_product_list.dart';
import 'package:loopdemo/features/home/widgets/search/home_search_section.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/di/di_provider.dart';
import 'package:loopdemo/shared/routing_manager.dart';
import 'package:loopdemo/shared/widgets/buttons/hamberger_icon_action_button.dart';
import 'package:loopdemo/shared/widgets/buttons/selectable_text_button.dart';
import 'package:loopdemo/shared/widgets/icons/floating_icon.dart';
import 'package:loopdemo/shared/widgets/screen_layout.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late final bloc = BlocProvider.of<HomeBloc>(context);

  @override
  void initState() {
    super.initState();
    bloc.onInitializeScreen();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenLayout(
      showLoadingIndicatorBehaviorSubject: bloc.showLoadingIndicatorBehaviorSubject,
      body: Stack(
        children: [
          Column(
            children: [
              HomeAppBar(),
              HomeSearchSection(),
              SizedBox(height: 16),
              HomeAppetizerDropdownButton(),
              SizedBox(height: 16),
              Expanded(child: HomeProductList()),
              HomeBottomPanel(),
            ],
          ),
          Positioned(
            left: 24,
            bottom: 84,
            child: HomeFloatingButton(),
          )
        ],
      ),
    );
  }
}
