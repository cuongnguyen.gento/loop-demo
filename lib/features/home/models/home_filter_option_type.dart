enum HomeFilterOptionType {
  atStore, takeAway, delivery
}

extension HomeFilterOptionTypeExtention on HomeFilterOptionType {
  String get displayText {
    switch(this) {
      case HomeFilterOptionType.atStore:
        return 'Ăn tại bàn';
      case HomeFilterOptionType.takeAway:
        return 'Mang đi';
      case HomeFilterOptionType.delivery:
        return 'Giao hàng';
    }
  }

  int get stackIndex {
    switch(this) {
      case HomeFilterOptionType.atStore:
        return 0;
      case HomeFilterOptionType.takeAway:
        return 1;
      case HomeFilterOptionType.delivery:
        return 2;
    }
  }
}