import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/models/brief_product_model.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/formatters/thounsand_formatter.dart';
import 'package:loopdemo/shared/widgets/cached_image.dart';
import 'package:loopdemo/shared/widgets/icons/add_icon.dart';

class HomeProductListTile extends StatelessWidget {
  final int index;
  final BriefProductModel briefProductModel;

  const HomeProductListTile({
    Key? key,
    required this.index,
    required this.briefProductModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      decoration: BoxDecoration(
        color: AppColorManager.white,
        borderRadius: BorderRadius.circular(8),
      ),
      child: Stack(
        children: [
          Row(
            children: [
              AspectRatio(
                aspectRatio: 1,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8),
                    topLeft: Radius.circular(8),
                  ),
                  child: CachedImage(url: briefProductModel.url),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    left: 16,
                    top: 12,
                    bottom: 4,
                    right: 4,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Trà sữa trân châu đường đen',
                        style: AppTextStyle(
                          fontSize: 13,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Row(
                        children: [
                          Row(
                            children: [
                              Text(
                                briefProductModel.remain.toString(),
                                style: AppTextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                ' còn lại',
                                style: AppTextStyle(
                                  fontSize: 9,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                            crossAxisAlignment: CrossAxisAlignment.end,
                          ),
                          Spacer(),
                          Container(
                            padding: EdgeInsets.symmetric(
                              vertical: 10,
                              horizontal: 20,
                            ),
                            decoration: BoxDecoration(
                              color: AppColorManager.oldLace,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(4)),
                            ),
                            child: Row(
                              children: [
                                Text(
                                  ThousandFormatter.format(
                                      briefProductModel.price),
                                  style: AppTextStyle(
                                    fontSize: 11,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                                SizedBox(width: 8),
                                AddIcon(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          Positioned.fill(
            child: Material(
              color: AppColorManager.transparent,
              child: InkWell(
                customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
                onTap: () =>
                    BlocProvider.of<HomeBloc>(context).onTapBriefProduct(index),
                child: Container(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
