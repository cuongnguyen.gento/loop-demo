import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/features/home/widgets/product/home_product_list_tile.dart';
import 'package:loopdemo/models/brief_product_model.dart';

class HomeProductList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeBloc>(context);
    return BlocBuilder<List<BriefProductModel>>(
      stream: bloc.briefProductModelsBehaviorSubject,
      builder: (context, briefProductModels) {
        return ListView.separated(
          keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
          padding: EdgeInsets.only(left: 24, right: 22),
          itemCount: briefProductModels.length,
          itemBuilder: (context, index) {
            return HomeProductListTile(
              index: index,
              briefProductModel: briefProductModels[index],
            );
          },
          separatorBuilder: (context, index) {
            return SizedBox(height: 8);
          },
        );
      },
    );
  }
}
