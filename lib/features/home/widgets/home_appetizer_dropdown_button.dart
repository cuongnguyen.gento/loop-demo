import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class HomeAppetizerDropdownButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24, right: 22),
      decoration: BoxDecoration(
        color: AppColorManager.white,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: DropdownButton<String>(
        icon: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.arrow_drop_down),
            SizedBox(width: 18),
          ],
        ),
        isExpanded: true,
        value: 'Khai vị',
        onChanged: (newValue) {},
        items: ['Khai vị', 'Khai vị 2', 'Khai vị 3', 'Khai vị 4']
            .map((String value) {
          return DropdownMenuItem<String>(
            value: value,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Text(
                value,
                style: AppTextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          );
        }).toList(),
        underline: Container(),
      ),
    );
  }
}
