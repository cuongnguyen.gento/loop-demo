import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/features/home/models/home_filter_option_type.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/buttons/hamberger_icon_action_button.dart';
import 'package:loopdemo/shared/widgets/buttons/selectable_text_button.dart';

class HomeAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeBloc>(context);
    return Container(
      color: AppColorManager.white,
      height: 64,
      child: Row(
        children: [
          Expanded(
            child: BlocBuilder<HomeFilterOptionType>(
                stream: bloc.selectedFilterOptionTypeBehaviorSubject,
                builder: (context, selectedFilterOptionType) {
                  return SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Container(
                      child: Row(
                        children: [
                          SelectableTextButton(
                            title: HomeFilterOptionType.atStore.displayText,
                            onTap: bloc.onTapAtStoreButton,
                            selected: selectedFilterOptionType ==
                                HomeFilterOptionType.atStore,
                          ),
                          SelectableTextButton(
                            title: HomeFilterOptionType.takeAway.displayText,
                            onTap: bloc.onTapTakeAwayButton,
                            selected: selectedFilterOptionType ==
                                HomeFilterOptionType.takeAway,
                          ),
                          SelectableTextButton(
                            title: HomeFilterOptionType.delivery.displayText,
                            onTap: bloc.onTapDeliveryButton,
                            selected: selectedFilterOptionType ==
                                HomeFilterOptionType.delivery,
                          ),
                        ],
                      ),
                    ),
                  );
                }),
          ),
          HambergerIconActionButton(
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
