import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class HomeSearchTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.all(Radius.circular(8)),
      child: Container(
        height: 40,
        child: TextField(
          expands: true,
          maxLines: null,
          minLines: null,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            border: InputBorder.none,

            hintText: 'Số thẻ',
            hintStyle: AppTextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w700,
            ),
            filled: true,
            fillColor: AppColorManager.quantityButton,
          ),
        ),
      ),
    );
  }
}
