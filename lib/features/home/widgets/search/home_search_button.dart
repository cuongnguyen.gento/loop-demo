import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/search_icon.dart';

class HomeSearchButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 40,
      height: 40,
      child: ElevatedButtonTheme(
        data: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: AppColorManager.quantityButton,
            onPrimary: AppColorManager.black,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12)),
            ),
          ),
        ),
        child: ElevatedButton(
          onPressed: () {},
          child: SearchIcon(),
        ),
      ),
    );
  }
}
