import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/features/home/models/home_filter_option_type.dart';
import 'package:loopdemo/features/home/widgets/search/home_search_button.dart';
import 'package:loopdemo/features/home/widgets/search/home_search_text_field.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/stacks/fade_indexed_stack.dart';

class HomeSearchSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<HomeBloc>(context);
    return Container(
      color: AppColorManager.white,
      padding: EdgeInsets.only(left: 24, right: 22, bottom: 15),
      child: BlocBuilder<HomeFilterOptionType>(
        stream: bloc.selectedFilterOptionTypeBehaviorSubject,
        builder: (context, selectedFilterOptionType) {
          return FadeIndexedStack(
            index: selectedFilterOptionType.stackIndex,
            children: [
              buildContent(),
              buildContent(),
              buildContent(),
            ],
          );
        }
      ),
    );
  }

  Widget buildContent() {
    return Row(
      children: [
        Expanded(child: HomeSearchTextField()),
        SizedBox(width: 12),
        HomeSearchButton(),
      ],
    );
  }
}
