import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/floating_icon.dart';

class HomeFloatingButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: 68,
          height: 68,
          decoration: BoxDecoration(
            color: AppColorManager.floatingShadow,
            borderRadius: BorderRadius.circular(20),
          ),
          padding: EdgeInsets.all(5),
          child: Container(
            decoration: BoxDecoration(
              color: AppColorManager.black,
              borderRadius: BorderRadius.circular(20),
            ),
            alignment: Alignment.center,
            child: FloatingIcon(),
          ),
        ),
        Positioned.fill(
          child: Material(
            color: AppColorManager.transparent,
            child: InkWell(
              customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
              onTap: () {},
            ),
          ),
        )
      ],
    );
  }
}
