import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class HomeCartButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 52,
      child: ElevatedButtonTheme(
        data: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: AppColorManager.yellow,
            onPrimary: AppColorManager.white,
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(8)),
            ),
          ),
        ),
        child: ElevatedButton(
          onPressed: () {},
          child: Row(
            children: [
              Text(
                'Giỏ hàng',
                style: AppTextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: AppColorManager.white),
              ),
              Expanded(
                child: Text(
                  ' - 0 món',
                  style: AppTextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.w600,
                      color: AppColorManager.white),
                ),
              ),
              Text(
                '- đ',
                style: AppTextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                    color: AppColorManager.white
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
