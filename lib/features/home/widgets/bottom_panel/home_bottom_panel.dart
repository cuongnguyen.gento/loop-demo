import 'package:flutter/material.dart';
import 'package:loopdemo/features/home/widgets/bottom_panel/home_calculation_button.dart';
import 'package:loopdemo/features/home/widgets/bottom_panel/home_cart_button.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/calculation_icon.dart';

class HomeBottomPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 12),
      color: AppColorManager.white,
      child: Row(
        children: [
          HomeCalculationButton(),
          SizedBox(width: 8),
          Expanded(child: HomeCartButton())
        ],
      ),
    );
  }
}
