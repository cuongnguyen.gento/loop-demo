import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/calculation_icon.dart';

class HomeCalculationButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 52,
      height: 52,
      decoration: BoxDecoration(
        color: AppColorManager.yellow,
        borderRadius: BorderRadius.all(Radius.circular(8)),
      ),
      child: Material(
        color: AppColorManager.transparent,
        child: InkWell(
          customBorder: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          onTap: () {},
          child: Center(child: CalculationIcon()),
        ),
      ),
    );
  }
}
