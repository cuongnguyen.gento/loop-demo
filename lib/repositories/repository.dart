import 'package:loopdemo/models/brief_product_model.dart';
import 'package:loopdemo/models/product_detail_model.dart';
import 'package:loopdemo/services/api_service.dart';

class Repository {

  final ApiService apiService;

  Repository({required this.apiService});

  Future<List<BriefProductModel>> getBriefProductModels() {
    return apiService.getBriefProductModels();
  }


  Future<ProductDetailModel> getProductDetail({required String productId}) {
    return apiService.getProductDetail(productId: productId);
  }

}