import 'package:flutter/material.dart';

typedef ProviderCreateFunction<T> = T Function(BuildContext context);

// ignore: must_be_immutable
class RepositoryProvider<T> extends StatefulWidget {
  final Widget child;
  late T repository;
  ProviderCreateFunction<T>? create;
  final bool usingRepositoryValue;

  RepositoryProvider({
    Key? key,
    required this.create,
    required this.child,
  })  : usingRepositoryValue = false,
        super(key: key);

  RepositoryProvider.value({
    Key? key,
    required this.repository,
    required this.child,
  })  : usingRepositoryValue = true,
        super(key: key);

  @override
  _RepositoryProviderState<T> createState() => _RepositoryProviderState<T>();

  static T of<T>(BuildContext context) {
    final blocProviderInherited = context.getElementForInheritedWidgetOfExactType<_RepositoryProviderInherited<T>>()?.widget as _RepositoryProviderInherited<T>;
    return blocProviderInherited.repository;
  }
}

class _RepositoryProviderState<T> extends State<RepositoryProvider<T>> {
  late T repository;

  @override
  void initState() {
    super.initState();
    if (widget.create != null) {
      repository = widget.create!(context);
    } else {
      repository = widget.repository;
    }
  }

  @override
  void didUpdateWidget(covariant RepositoryProvider<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.usingRepositoryValue) {
      repository = widget.repository;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _RepositoryProviderInherited<T>(
      repository: repository,
      child: widget.child,
    );
  }
}

class _RepositoryProviderInherited<T> extends InheritedWidget {
  _RepositoryProviderInherited({
    Key? key,
    required Widget child,
    required this.repository,
  }) : super(key: key, child: child);

  final T repository;

  @override
  bool updateShouldNotify(_RepositoryProviderInherited oldWidget) => false;
}