import 'package:flutter/material.dart';

extension BuildContextExtension on BuildContext {
  T bloc<T extends BlocBase>() {
    return BlocProvider.of<T>(this);
  }
}

typedef BlocCreateFunction<T> = T Function(BuildContext context);

abstract class BlocBase {
  void dispose() {}
}

// ignore: must_be_immutable
class BlocProvider<T extends BlocBase> extends StatefulWidget {
  final Widget child;
  late T bloc;
  BlocCreateFunction<T>? create;
  final bool usingBlocValue;

  BlocProvider({
    Key? key,
    required this.create,
    required this.child,
  })  : usingBlocValue = false,
        super(key: key);

  BlocProvider.value({
    Key? key,
    required this.bloc,
    required this.child,
  })  : usingBlocValue = true,
        super(key: key);

  @override
  _BlocProviderState<T> createState() => _BlocProviderState<T>();

  static T of<T extends BlocBase>(BuildContext context) {
    final blocProviderInherited = context.getElementForInheritedWidgetOfExactType<_BlocProviderInherited<T>>()?.widget as _BlocProviderInherited<T>;
    return blocProviderInherited.bloc;
  }
}

class _BlocProviderState<T extends BlocBase> extends State<BlocProvider<T>> {
  late T bloc;

  @override
  void initState() {
    super.initState();
    if (widget.create != null) {
      bloc = widget.create!(context);
    } else {
      bloc = widget.bloc;
    }
  }

  @override
  void dispose() {
    if (widget.usingBlocValue == false) {
      bloc.dispose();
    }
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant BlocProvider<T> oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.usingBlocValue) {
      bloc = widget.bloc;
    }
  }

  @override
  Widget build(BuildContext context) {
    return _BlocProviderInherited<T>(
      bloc: bloc,
      child: widget.child,
    );
  }
}

class _BlocProviderInherited<T> extends InheritedWidget {
  _BlocProviderInherited({
    Key? key,
    required Widget child,
    required this.bloc,
  }) : super(key: key, child: child);

  final T bloc;

  @override
  bool updateShouldNotify(_BlocProviderInherited oldWidget) => false;
}

class BlocBuilder<S> extends StatelessWidget {
  final S? initialData;
  final Stream<S> stream;
  final Widget Function(BuildContext context, S value) builder;

  BlocBuilder({
    this.initialData,
    required this.stream,
    required this.builder,
  });

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<S>(
      initialData: initialData,
      stream: stream,
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.active:
            return builder(context, snapshot.data!);
          default:
            return Container();
        }
      },
    );
  }
}
