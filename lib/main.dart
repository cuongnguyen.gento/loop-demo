import 'package:flutter/material.dart';
import 'package:loopdemo/shared/routing_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Loop Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      navigatorKey: RoutingManager.navigatorKey,
      initialRoute: RouteName.home,
      onGenerateRoute: RoutingManager.onGenerateRoute,
    );
  }
}