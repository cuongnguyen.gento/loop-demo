import 'dart:math';

import 'package:dio/dio.dart';
import 'package:loopdemo/models/brief_product_model.dart';
import 'package:loopdemo/models/product_detail_model.dart';
import 'package:loopdemo/models/product_ice_type.dart';
import 'package:loopdemo/models/product_material_model.dart';
import 'package:loopdemo/models/product_size_type.dart';
import 'package:loopdemo/models/product_sugar_type.dart';
import 'package:loopdemo/models/voucher_model.dart';

class ApiService {
  static final baseOptions = BaseOptions(
      connectTimeout: 5000, receiveTimeout: 5000, baseUrl: "https://reqres.in");

  final Dio _dio = Dio(
    baseOptions,
  );

  Future<Response?> _get(String path) async {
    //try catch is a must
    try {
      return await _dio.get(path);
    } catch (error) {
      print(convertErrorToHumanReadableMessage(error));
      return null;
    }
  }

  Future<Response?> _post({
    required String path,
    required Map<String, dynamic> body,
  }) async {
    //try catch is a must
    try {
      return await _dio.post(path, data: body);
    } catch (error) {
      print(convertErrorToHumanReadableMessage(error));
      return null;
    }
  }

  Future<Response?> _path({
    required String path,
    required Map<String, dynamic> body,
  }) async {
    //try catch is a must
    try {
      return await _dio.patch(path, data: body);
    } catch (error) {
      print(convertErrorToHumanReadableMessage(error));
      return null;
    }
  }

  String convertErrorToHumanReadableMessage(dynamic error) {
    String errorDescription = "";
    if (error is DioError) {
      final DioError dioError = error;
      switch (dioError.type) {
        case DioErrorType.cancel:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.connectTimeout:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.other:
          errorDescription =
              "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.receiveTimeout:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.response:
          errorDescription =
              "Received invalid status code: ${error.response!.statusCode}";
          break;
        case DioErrorType.sendTimeout:
          errorDescription = "Send timeout in connection with API server";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return "error: " + errorDescription;
  }

  Future<List<BriefProductModel>> getBriefProductModels() async {
    // simulate network response time
    await Future.delayed(Duration(seconds: 2));
    // dummy data
    return List.generate(
      20,
      (index) => BriefProductModel(
        url:
            'https://cdnimg.webstaurantstore.com/images/products/large/514524/1907864.jpg',
        title: 'Trà sữa trân châu đường đen',
        remain: 121,
        price: (Random().nextInt(9) + 2) * 10000,
      ),
    );
  }

  Future<ProductDetailModel> getProductDetail({
    required String productId,
  }) async {
    // simulate network response time
    await Future.delayed(Duration(seconds: 2));
    // dummy data
    return ProductDetailModel(
      id: productId,
      url:
          'https://cdnimg.webstaurantstore.com/images/products/large/514524/1907864.jpg',
      title: 'Trà đào cam sả nhưng làm ở Phúc Long nè cute không',
      price: 35000,
      remain: 121,
      sizeType: ProductSizeType.s,
      sugarType: ProductSugarType.hundred,
      iceType: ProductIceType.hundred,
      quantity: 25,
      materialModels: [
        ProductMaterialModel(
          quantity: 0,
          name: 'Trân châu đen',
          price: 5000,
        ),
        ProductMaterialModel(
          name: 'Trân châu trắng',
          price: 5000,
          quantity: 12,
        ),
        ProductMaterialModel(
          name: 'Lô hội',
          price: 5000,
          quantity: 3,
        ),
        ProductMaterialModel(
          quantity: 0,
          name: 'Pudding',
          price: 5000,
        ),
        ProductMaterialModel(
          quantity: 0,
          name: 'Kem sữa',
          price: 5000,
        ),
        ProductMaterialModel(
          quantity: 0,
          name: 'Thạch lô hội',
          price: 5000,
        ),
      ],
      voucherModels: [
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
        VoucherModel(
          code: 'HESOIDONG',
          title: 'Giảm giá 20% tối đa 30k ...',
          endDate: DateTime.now().add(Duration(days: 3)),
        ),
      ],
    );
  }
}
