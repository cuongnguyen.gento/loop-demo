import 'package:intl/intl.dart';

class ThousandFormatter {
  static String format(int number) {
    return NumberFormat.decimalPattern('vi').format(number);
  }
}