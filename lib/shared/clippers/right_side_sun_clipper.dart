import 'package:flutter/material.dart';

class RightSideSunClipper extends CustomClipper<Path> {
  final double radius = 8;

  @override
  Path getClip(Size size) {
    Path path = Path();
    path.moveTo(0, 0.0);
    path.lineTo(0.0, size.height);
    path.lineTo(size.width - radius, size.height);
    path.arcToPoint(Offset(size.width, size.height - radius),
        clockwise: true, radius: Radius.circular(radius));
    path.lineTo(size.width, radius);
    path.arcToPoint(Offset(size.width - radius, 0.0),
        clockwise: true, radius: Radius.circular(radius));
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}