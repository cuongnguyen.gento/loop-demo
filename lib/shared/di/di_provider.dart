import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/features/home/home_bloc.dart';
import 'package:loopdemo/features/home/home_screen.dart';
import 'package:loopdemo/features/product/detail/product_detail_bloc.dart';
import 'package:loopdemo/features/product/detail/product_detail_screen.dart';
import 'package:loopdemo/repositories/repository.dart';
import 'package:loopdemo/services/api_service.dart';
import 'package:loopdemo/shared/routing_manager.dart';

class DiProvider {
  static final routingManager = RoutingManager();

  static final repository = Repository(
    apiService: ApiService(),
  );

//  Route
  static MaterialPageRoute resolveHomePageRoute() {
    return MaterialPageRoute(
      builder: (context) {
        return BlocProvider(
          create: (context) => HomeBloc(
            repository: DiProvider.repository,
            routingManager: DiProvider.routingManager,
          ),
          child: HomeScreen(),
        );
      },
    );
  }

  static MaterialPageRoute resolveProductDetailRoute({
    required String productId,
  }) {
    return MaterialPageRoute(builder: (context) {
      return BlocProvider(
        create: (context) => ProductDetailBloc(
          productId: productId,
          routingManager: DiProvider.routingManager,
          repository: DiProvider.repository,
        ),
        child: ProductDetailScreen(),
      );
    });
  }
}
