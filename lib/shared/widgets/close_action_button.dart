import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class CloseActionButton extends StatelessWidget {
  final VoidCallback onTap;

  const CloseActionButton({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColorManager.transparent,
      child: InkWell(
        customBorder: CircleBorder(),
        onTap: onTap,
        child: Container(
          margin: EdgeInsets.all(16),
          width: 36,
          height: 36,
          decoration: BoxDecoration(
            color: AppColorManager.closeActionButton,
            borderRadius: BorderRadius.all(Radius.circular(12)),
          ),
          child: Icon(
            Icons.close,
            size: 15,
            color: AppColorManager.white,
          ),
        ),
      ),
    );
  }
}
