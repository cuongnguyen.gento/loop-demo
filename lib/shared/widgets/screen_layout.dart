import 'package:flutter/material.dart';
import 'package:loopdemo/core/BlocProvider.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:rxdart/rxdart.dart';

class ScreenLayout extends StatelessWidget {
  final BehaviorSubject<bool>? showLoadingIndicatorBehaviorSubject;
  final Widget body;
  final PreferredSizeWidget? appBar;

  const ScreenLayout({
    Key? key,
    this.showLoadingIndicatorBehaviorSubject,
    required this.body,
    this.appBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Stack(
        children: [
          Scaffold(
            appBar: appBar,
            body: SafeArea(child: body),
            backgroundColor: AppColorManager.snow,
          ),
          if (showLoadingIndicatorBehaviorSubject != null)
            BlocBuilder<bool>(
              stream: showLoadingIndicatorBehaviorSubject!,
              builder: (context, value) {
                return value ? AbsorbPointer(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                ) : Container();
              },
            ),
        ],
      ),
    );
  }
}
