import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class AppIcon extends StatelessWidget {
  final String fileName;
  final double height;
  final double width;
  final Color color;

  const AppIcon({
    Key? key,
    required this.fileName,
    required this.height,
    required this.width,
    this.color = AppColorManager.black,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return SvgPicture.asset(
      "assets/icons/$fileName.svg",
      height: height,
      width: width,
      color: color,
    );
  }
}
