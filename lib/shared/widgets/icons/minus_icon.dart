import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class MinusIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'minus',
      height: 1,
      width: 10,
    );
  }
}
