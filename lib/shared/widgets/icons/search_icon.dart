import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class SearchIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'search',
      height: 11,
      width: 11,
      color: AppColorManager.black,
    );
  }
}
