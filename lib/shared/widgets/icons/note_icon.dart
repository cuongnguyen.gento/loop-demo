import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class NoteIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'note',
      height: 16,
      width: 16,
    );
  }
}
