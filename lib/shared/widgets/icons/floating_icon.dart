import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class FloatingIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'floating',
      height: 16,
      width: 28,
      color: AppColorManager.white,
    );
  }
}
