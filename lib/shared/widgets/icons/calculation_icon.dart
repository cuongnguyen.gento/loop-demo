import 'package:flutter/material.dart';

class CalculationIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset(
        'assets/calculation.png',
        width: 28,
        height: 28,
      ),
    );
  }
}
