import 'package:flutter/material.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class HambergerIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'hamberger',
      height: 14,
      width: 14,
    );
  }
}
