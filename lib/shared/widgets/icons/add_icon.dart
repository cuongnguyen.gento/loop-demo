import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class AddIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'add',
      height: 8,
      width: 8,
      color: AppColorManager.yellow,
    );
  }
}
