import 'package:flutter/material.dart';
import 'package:loopdemo/shared/widgets/icons/app_icon.dart';

class DiscountIcon extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AppIcon(
      fileName: 'discount',
      width: 24,
      height: 24,
    );
  }
}
