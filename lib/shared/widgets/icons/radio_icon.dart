import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class RadioIcon extends StatelessWidget {
  final bool selected;

  const RadioIcon({Key? key, required this.selected}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 16,
      height: 16,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: selected ? AppColorManager.yellow : AppColorManager.black,
          width: 1,
        ),
      ),
      child: Center(
        child: selected
            ? Container(
                width: 10,
                height: 10,
                decoration: BoxDecoration(
                  color: AppColorManager.yellow,
                  shape: BoxShape.circle,
                ),
              )
            : Container(),
      ),
    );
  }
}
