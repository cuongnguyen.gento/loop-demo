import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';
import 'package:loopdemo/shared/widgets/icons/minus_icon.dart';

class QuantityControl extends StatelessWidget {

  final int quantity;
  final VoidCallback onTapMinusButton;
  final  VoidCallback onTapAddButton;

  const QuantityControl({
    Key? key,
    required this.quantity,
    required this.onTapMinusButton,
    required this.onTapAddButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButtonTheme(
      data: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: AppColorManager.quantityButton,
          onPrimary: AppColorManager.black,
          padding: EdgeInsets.zero,
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(4)),
          ),
        ),
      ),
      child: Container(
        height: 48,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              width: 48,
              child: ElevatedButton(
                onPressed: onTapMinusButton,
                child: MinusIcon(),
              ),
            ),
            Container(
              width: 48,
              alignment: Alignment.center,
              child: Text(
                quantity.toString(),
                style: AppTextStyle.highlight(fontSize: 14),
              ),
            ),
            Container(
              width: 48,
              child: ElevatedButton(
                onPressed: onTapAddButton,
                child: Icon(
                  Icons.add,
                  size: 10,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
