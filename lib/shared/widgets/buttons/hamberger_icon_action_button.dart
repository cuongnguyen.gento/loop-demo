import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/widgets/icons/hamberger_icon.dart';

class HambergerIconActionButton extends StatelessWidget {

  final VoidCallback onTap;

  const HambergerIconActionButton({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 50,
      height: 50,
      color: AppColorManager.white,
      child: Material(
        color: AppColorManager.white,
        child: InkWell(
          customBorder: CircleBorder(),
          onTap: onTap,
          child: Center(child: HambergerIcon()),
        ),
      ),
    );
  }
}
