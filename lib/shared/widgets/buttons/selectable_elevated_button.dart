import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class SelectableElevatedButton extends StatelessWidget {
  final String text;
  final bool selected;
  final VoidCallback onTap;

  const SelectableElevatedButton({
    Key? key,
    required this.onTap,
    required this.text,
    required this.selected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: selected
          ? AppColorManager.oldLace
          : AppColorManager.snow,
      borderRadius: BorderRadius.all(Radius.circular(4)),
      child: InkWell(
        onTap: onTap,
        child: Container(
          width: 76,
          height: 44,
          padding: EdgeInsets.symmetric(vertical: 12),
          alignment: Alignment.center,
          child: Text(
            text,
            style: selected
                ? AppTextStyle.highlight(fontSize: 15)
                : AppTextStyle.unhighlight(fontSize: 15),
          ),
        ),
      ),
    );
  }
}
