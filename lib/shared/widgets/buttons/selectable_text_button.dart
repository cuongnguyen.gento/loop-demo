import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';
import 'package:loopdemo/shared/app_text_style.dart';

class SelectableTextButton extends StatelessWidget {
  final bool selected;
  final String title;
  final VoidCallback onTap;

  const SelectableTextButton({
    Key? key,
    required this.selected,
    required this.title,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButtonTheme(
      data: TextButtonThemeData(
        style: TextButton.styleFrom(
          padding: EdgeInsets.symmetric(horizontal: 28),
          textStyle: AppTextStyle(
            fontSize: 13,
            fontWeight: selected ? FontWeight.w700 : FontWeight.w500,
          ),
          primary: selected ? AppColorManager.yellow : AppColorManager.dimGray,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(18)),
          ),
        ),
      ),
      child: TextButton(
        onPressed: onTap,
        child: Text(title),
      ),
    );
  }
}
