import 'package:flutter/material.dart';
import 'package:loopdemo/shared/widgets/buttons/selectable_elevated_button.dart';

class SingleSelectionButton {
  final String displayText;

  SingleSelectionButton({required this.displayText});
}

class SingleSelectionButtonsList extends StatelessWidget {
  final List<SingleSelectionButton> values;
  final SingleSelectionButton selectedValue;
  final ValueChanged<int> onTap;

  const SingleSelectionButtonsList({
    Key? key,
    required this.values,
    required this.selectedValue,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      child: LayoutBuilder(
        builder: (context, constraints) {
          return Container(
            child: ListView.separated(
              scrollDirection: Axis.horizontal,
              itemCount: values.length,
              itemBuilder: (context, index) {
                return SelectableElevatedButton(
                  text: values[index].displayText,
                  selected: values[index].displayText == selectedValue.displayText,
                  onTap: () {
                    onTap(index);
                  },
                );
              },
              separatorBuilder: (context, index) {
                return SizedBox(width: 8);
              },
            ),
          );
        },
      ),
    );
  }
}
