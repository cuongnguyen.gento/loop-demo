
import 'package:flutter/material.dart';

class AppColorManager {
  static const snow = Color(0xffFAFAFA);
  static const closeActionButton = Color.fromARGB(204, 65, 65, 65);
  static const white = Colors.white;
  static const whiteSmoke = Color(0xffF4F4F4);
  static const transparent = Colors.transparent;
  static const black = Color(0xff414141);
  static const dimGray = Color(0xff656565);
  static const nobel = Color(0xff9A9A9A);
  static const yellow = Color(0xffFEAA00);
  static const orange = Color(0xffFFAB00);
  static const oldLace = Color(0xffFFF6E3);
  static const quantityButton = Color(0xffF9F9F9);
  static const floatingShadow = Color.fromARGB(51, 123, 136, 176);
}