import 'package:flutter/material.dart';
import 'package:loopdemo/shared/DI/di_provider.dart';

class RouteName {
  static const home = 'home';
  static const productDetail = 'productDetail';
}

class RoutingPageType {
  final String routeName;
  Object? arguments;

  RoutingPageType.home() : routeName = RouteName.home;

  RoutingPageType.productDetail({required String productId})
      : routeName = RouteName.productDetail,
        arguments = productId;
}

class RoutingManager {

  static final navigatorKey = GlobalKey<NavigatorState>();

  static NavigatorState get navigatorState => navigatorKey.currentState!;

  void pop<T extends Object>([T? result]) {
    navigatorState.pop<T>(result);
  }

  Future<T?> push<T extends Object?>(
      RoutingPageType routingPageType) async {
    final result = await navigatorState.pushNamed(
      routingPageType.routeName,
      arguments: routingPageType.arguments,
    );
    return result as T?;
  }

  Future<T?> pushReplacement<T extends Object?>(
      RoutingPageType routingPageType) async {
    final result = await navigatorState.pushReplacementNamed(
      routingPageType.routeName,
      arguments: routingPageType.arguments,
    );
    return result as T?;
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case RouteName.home:
        return DiProvider.resolveHomePageRoute();
      case RouteName.productDetail:
        return DiProvider.resolveProductDetailRoute(
          productId: settings.arguments as String,
        );
    }
    return _unknownMaterialPageRoute;
  }

  static MaterialPageRoute get _unknownMaterialPageRoute {
    return MaterialPageRoute(builder: (context) {
      return Scaffold(
        appBar: AppBar(title: Text("UnknownRoute")),
        body: Text("UnknownRoute"),
      );
    });
  }
}
