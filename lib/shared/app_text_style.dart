import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class AppTextStyle extends TextStyle {

  AppTextStyle({
    required double fontSize,
    Color color = AppColorManager.black,
    FontWeight fontWeight = FontWeight.normal,
    FontStyle fontStyle = FontStyle.normal,
  }) : super(
          fontSize: fontSize,
          color: color,
          fontWeight: fontWeight,
          fontStyle: fontStyle,
        );

  AppTextStyle.enable({required double fontSize})
      : this(
          fontSize: fontSize,
          fontWeight: FontWeight.w600,
        );

  AppTextStyle.disable({required double fontSize})
      : this(
          fontSize: fontSize,
          color: AppColorManager.nobel,
          fontWeight: FontWeight.w400,
        );

  AppTextStyle.highlight({required double fontSize})
      : this(
    fontSize: fontSize,
    color: AppColorManager.orange,
    fontWeight: FontWeight.w700,
  );

  AppTextStyle.unhighlight({required double fontSize})
      : this(
    fontSize: fontSize,
    color: AppColorManager.dimGray,
    fontWeight: FontWeight.w500,
  );
}
