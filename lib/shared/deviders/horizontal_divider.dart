import 'package:flutter/material.dart';
import 'package:loopdemo/shared/app_color_manager.dart';

class HorizontalDivider extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1,
      color: AppColorManager.nobel,
    );
  }
}
