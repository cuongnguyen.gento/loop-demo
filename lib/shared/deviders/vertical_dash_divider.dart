import 'package:flutter/material.dart';
import 'package:loopdemo/shared/painters/vertical_dash_painter.dart';

class VerticalDashDivider extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1,
      height: double.infinity,
      child: CustomPaint(
        painter: VerticalDashPainter(),
      ),
    );
  }
}
